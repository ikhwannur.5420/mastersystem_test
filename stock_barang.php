<?php
include 'koneksi.php';
date_default_timezone_set("Asia/Jakarta");

$q_stok = mysqli_query($koneksi, "select st.kode, pr.nama, st.quantity from stok as st left join produk as pr on st.kode = pr.kode");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mastersystem - Stock Barang</title>
</head>

<body>
    <h2>Stock barang</h2>

    <a href="index.php">Kembali</a>
    <table border="1">
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama Barang</th>
            <th>Quantity</th>
        </tr>
        <?php
        $no = 1;
        while ($data = mysqli_fetch_assoc($q_stok)) {
        ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $data['kode']; ?></td>
                <td><?php echo $data['nama']; ?></td>
                <td><?php echo $data['quantity']; ?></td>
            </tr>
        <?php
        }
        ?>
    </table>
</body>

</html>