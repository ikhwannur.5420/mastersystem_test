/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.21-MariaDB : Database - mastersystem_test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mastersystem_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `mastersystem_test`;

/*Table structure for table `keluar_barang` */

DROP TABLE IF EXISTS `keluar_barang`;

CREATE TABLE `keluar_barang` (
  `nomor_srt_jalan` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `penerima` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nomor_srt_jalan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `keluar_barang` */

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `produk` */

insert  into `produk`(`kode`,`nama`) values 
('IND0001','Indomie'),
('MBL0002','Mobil Toyota'),
('TVD0001','TV Android');

/*Table structure for table `stok` */

DROP TABLE IF EXISTS `stok`;

CREATE TABLE `stok` (
  `kode` varchar(20) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `stok` */

/*Table structure for table `tanda_terima` */

DROP TABLE IF EXISTS `tanda_terima`;

CREATE TABLE `tanda_terima` (
  `nomor_srt_jalan` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `penerima` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nomor_srt_jalan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tanda_terima` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
