<?php
include 'koneksi.php';
date_default_timezone_set("Asia/Jakarta");

$q_produk = mysqli_query($koneksi, "select * from produk");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mastersystem - Penerimaan barang</title>
</head>

<body>
    <h2>Form Penerimaan Barang</h2>

    <form action="proses_penerimaan_barang.php" method="post">
        <a href="index.php">Kembali</a>
        <table>
            <tr>
                <td>Nama Barang : </td>
                <td>
                    <select name="barang" required>
                        <option value="">--Pilih Barang--</option>
                        <?php
                        while ($data = mysqli_fetch_assoc($q_produk)) {
                        ?>
                            <option value="<?php echo $data['kode']; ?>"><?php echo $data['nama']; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Jumlah Stok : </td>
                <td><input type="number" name="jumlah_stok" min="0" step="0" require></td>
            </tr>
            <tr>
                <td><input type="submit" value="submit"></td>
            </tr>
        </table>
    </form>
</body>

</html>